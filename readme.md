# Thông tin cá nhân
- MSSV: 1512557
- Họ tên: Phan Trọng Thuyên
# Các chức năng đã làm được
## Yêu cầu chính

1. Thêm vào danh sách một mục chi, gồm 3 thành phần: Loại chi tiêu, nội dung chi tiêu và số tiền đã chi ra
2. Có các textbox để nhập dữ liệu, sau khi nhập thực hiện thêm vào list view bên cạnh và hiển thị cho người dùng
3. Xem lại danh sách từ list view, lưu và nạp tập tin text
- Khi chương trình chạy lên thì tự động nạp danh sách chi tiêu từ tập tin text và hiển thị trên list view
- Khi chương trình thoát thì tự động lưu danh sách mới vào tập tin text
- Undo những thứ thêm vào
- Delete item được chọn khi bấm Delete
4. Vẽ biểu đồ biểu diễn trực quan tỉ lệ tiêu xài

## Yêu cầu nâng cao
1. Tập tin lưu trữ các khoản chi tiêu hỗ trợ lưu và nạp Tiếng Việt.
2. Vẽ biểu đồ hình tròn để biểu thị tỉ lệ chi tiêu

## Các luồng sự kiện chính
> Chạy chương trình, nạp dữ liệu từ file nếu có
> Hiển thị textbox, combobox bên trái để nhập liệu được gom vào một groupbox
> Hiển thị list view bên phải hiển thị trực quan dữ liệu nhập vào
> Thông kê biểu đồ được hiển thị ở góc dưới bên trái, hiển thị biểu đồ tròn, chú thích và tổng tiền đã chi ra
> Người dùng thoát chương trình lưu lại dữ liệu xuống file
> Nhập dữ liệu vào textbox hoặc chọn từ combobox, nhấn Thêm để thêm dữ liệu, ở textbox tiền, không cho phép nhập chữ

## Hướng dẫn sử dụng
> Người dùng mở chương trình, chọn loại chi tiêu, nhập nội dung chi tiết và số tiền tương ứng
> Ấn nút Thêm để thêm vào listview
> Biểu đồ hiển thị và đưa ra thống kê cho người dùng
> Thoát chương trình, dữ liệu sẽ tự động lưu xuống.

## Yêu cầu khác
> Link repo: https://billhcmus@bitbucket.org/billhcmus/personal-finance.git

> Link video: https://youtu.be/vrbAdTRoOTY

> Nền tảng build: Visual Studio 2017
